class Attribute {
    /**
     * Base class for GURPS character attributes
     * @param {String} name Name of the attribute
     * @param {Number} points Number of points for the attribute
     * @param {Number} [level] level Level of the attribute
     */
    constructor(name, points, level) {
        this.name = name;
        this.points = points;
        this.level = level;
    }
}

class BasicAttribute extends Attribute {};
class SecondaryCharacteristic extends Attribute {};
class Trait extends Attribute {};
class Advantage extends Trait {};
class Disadvantage extends Trait {};
class Perk extends Advantage {
    constructor(name) {
        super(name, 1);
    }
}
class Quirk extends Disadvantage {
    constructor(name) {
        super(name, -1);
    }
}
class Skill extends Attribute {};
// Because spells are really just skills in disguise.
class Spell extends Skill {};

var basicAttributeOrder = ["ST", "DX", "IQ", "HT"];
var secondaryCharacteristicOrder = ["HP", "Per", "Will", "FP"]

var testCharacter = [
    new BasicAttribute( )
]